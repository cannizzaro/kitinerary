/*
    Copyright (C) 2018 Volker Krause <vkrause@kde.org>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef KITINERARY_COUNTRYDB_P_H
#define KITINERARY_COUNTRYDB_P_H

#include "countrydb.h"

namespace KItinerary {
namespace KnowledgeDb {
/** UIC code lookup table entries. */
struct UicCountryCodeMapping {
    uint16_t uicCode;
    CountryId isoCode;
};

/** ISO 3166-1 alpha 3 to ISO 3166-1 alpha 2 mapping. */
struct IsoCountryCodeMapping {
    CountryId3 iso3Code;
    CountryId  iso2Code;
};

}
}

#endif // KITINERARY_COUNTRYDB_P_H

